var express = require('express');
var router = express.Router();
var csv = require('fast-csv');
var request = require('request');
var mdb = require('moviedb')(require('../config.js').TMDB_APIKEY);

router.get('/', (req, res) => {
	getTmdbIdFromLbd('http://letterboxd.com/film/millennium-actress')
		.then((lbdId) => {
			res.render('index', {title: lbdId});
		})
	;
});

function update(csvFile) {
	fs.createReadStream(csvFile)
		.pipe(csv())
		.on('data', (data) => { console.log(data); })
	;
}

function getTmdbIdFromLbd(lbdUri) {
	return new Promise((resolve) => {
		request(lbdUri, (err, res, body) => {
			resolve(/http:\/\/www.themoviedb.org\/movie\/(\d+)\//.exec(body)[1]);
		});
	});
}

module.exports = router;
