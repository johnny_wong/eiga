var express = require('express');
var router = express.Router();
var mdb = require('moviedb')('e3830af8664e37ec900a0cf12d51ed3f');

router.get('/', (req, res) => { res.render('index', {title: 'Express'}); });

router.get('/info/:id', (req, res) => {
	mdb.movieInfo({id: req.params.id}, (err, val) => {
		res.json(val);
	});
});

router.get('/credits/:id', (req, res) => {
	mdb.movieCredits({id: req.params.id}, (err, val) => {
		res.json(val);
	});
});

module.exports = router;

/*
 * TODO: read csv
 * TODO: parse csv into db
 * TODO: query tmdb for more information on each csv row
 * TODO: create basic form with dropdowns
 * TODO:
 */
